<?php

/**
 * @file media_tv3/includes/providers/tv3.inc
 * Provide support for the UStream provider to the emfield.module.
 */

/**
 * hook emvideo_PROVIDER_info
 * this returns information relevant to a specific 3rd party video provider
 * @return
 *   an array of strings requested by various admin and other forms
 *   'name' => the translated name of the provider
 *   'url' => the url to the main page for the provider
 *   'settings_description' => a description of the provider that will be posted in the admin settings form
 *   'supported_features' => an array of rows describing the state of certain supported features by the provider.
 *      These will be rendered in a table, with the columns being 'Feature', 'Supported', 'Notes'.
 */
function emvideo_tv3_info() {
  $features = array(
    array(t('Autoplay'), t('No'), ''),
    array(t('RSS Attachment'), t('No'), ''),
    array(t('Thumbnails'), t('Yes'), t('')),
  );
  return array(
    'provider' => 'tv3',
    'name' => t('tv3.cat'),
    'url' => MEDIA_TV3_MAIN_URL,
    'settings_description' => t('These settings specifically affect videos displayed from !tv3.', array('!tv3' => l($name, MEDIA_TV3_MAIN_URL, array('target' => '_blank')) )),
    'supported_features' => $features,
  );
}

/**
 * hook emvideo_PROVIDER_settings
 * this should return a subform to be added to the emvideo_settings() admin settings page.
 * note that a form field will already be provided, at $form['PROVIDER'] (such as $form['vimeo'])
 * so if you want specific provider settings within that field, you can add the elements to that form field.
 */
function emvideo_tv3_settings() {
}

/**
 * hook emvideo_PROVIDER_extract
 * this is called to extract the video code from a pasted URL or embed code.
 * @param $embed
 *   an optional string with the pasted URL or embed code
 * @return
 *   either an array of regex expressions to be tested, or a string with the video code to be used
 *   if the hook tests the code itself, it should return either the string of the video code (if matched), or an empty array.
 *   otherwise, the calling function will handle testing the embed code against each regex string in the returned array.
 */
function emvideo_tv3_extract($embed = '') {
  // http://www.tv3.cat/3alacarta/#/videos570233
  return array(
    '@www.tv3\.cat/3alacarta/#/videos/(\d+)@i',
    '@flashvars="loc=%2F&amp;autoplay=false&amp;vid=(\d+)@i',
  );
}

/**
 * hook emvideo_PROVIDER_embedded_link($video_code)
 * returns a link to view the video at the provider's site
 *  @param $video_code
 *    the string containing the video to watch
 *  @return
 *    a string containing the URL to view the video at the original provider's site
 */
function emvideo_tv3_embedded_link($video_code) {
  return 'http://www.tv3.cat/3alacarta/#/videos/' . $video_code;
}

/**
 * hook emvideo_PROVIDER_video
 * this actually displays the full/normal-sized video we want, usually on the default page view
 *  @param $embed
 *    the video code for the video to embed
 *  @param $width
 *    the width to display the video
 *  @param $height
 *    the height to display the video
 *  @param $field
 *    the field info from the requesting node
 *  @param $item
 *    the actual content from the field
 *  @return
 *    the html of the embedded video
 */
function emvideo_tv3_video($embed, $width, $height, $field, $item, $node, $autoplay, $options = array()) {
  $output = theme('emvideo_tv3_flash', $embed, $width, $height, $autoplay, $options);
  return $output;
}

/**
 * hook emvideo_PROVIDER_video
 * this actually displays the preview-sized video we want, commonly for the teaser
 *  @param $embed
 *    the video code for the video to embed
 *  @param $width
 *    the width to display the video
 *  @param $height
 *    the height to display the video
 *  @param $field
 *    the field info from the requesting node
 *  @param $item
 *    the actual content from the field
 *  @return
 *    the html of the embedded video
 */
function emvideo_tv3_preview($embed, $width, $height, $field, $item, $node, $autoplay, $options = array()) {
  $output = theme('emvideo_tv3_flash', $embed, $width, $height, $autoplay, $options);
  return $output;
}

/**
 * hook emvideo_PROVIDER_thumbnail
 * returns the external url for a thumbnail of a specific video
 * TODO: make the args: ($embed, $field, $item), with $field/$item provided if we need it, but otherwise simplifying things
 *  @param $field
 *    the field of the requesting node
 *  @param $item
 *    the actual content of the field from the requesting node
 *  @return
 *    a URL pointing to the thumbnail
 */
function emvideo_tv3_thumbnail($field, $item, $formatter, $node, $width, $height, $options = array()) {
  $tn = $item['data']['imgsrc'];
  return $tn;
}

/**
 * this is a wrapper for emvideo_request_xml that includes tv3's api key
 */
function emvideo_tv3_request($code = '', $cached = TRUE) {
  $url = "http://www.tv3.cat/pvideo/FLV_bbd_dadesItem.jsp?idint=$code";

  //$ret = module_invoke('emfield', 'request_xml', 'tv3', $url, array(), $cached, FALSE, $code, TRUE);
  $ret = module_invoke('emfield', 'retrieve_xml', $url, FALSE, $cached == FALSE);
  return $ret;
}

/**
 *  Implement hook emvideo_PROVIDER_data_version().
 */
function emvideo_tv3_data_version() {
  return MEDIA_TV3_DATA_VERSION;
}

/**
 * hook emfield_PROVIDER_data
 *
 * provides an array to be serialised and made available with $item elsewhere
 */
function emvideo_tv3_data($field, $item) {
  $data = emvideo_tv3_request($item['value']);
  $data['tv3_api_version'] = $data['emvideo_data_version'] = MEDIA_TV3_DATA_VERSION;
  return $data;
}

/**
 * hook emvideo_PROVIDER_duration($item)
 * Returns the duration of the video in seconds.
 *  @param $item
 *    The video item itself, which needs the $data array.
 *  @return
 *    The duration of the video in seconds.
 */
function emvideo_tv3_duration($item) {
  if (!isset($item['data']['tv3_api_version'])) {
    $item['data'] = emvideo_tv3_data(NULL, $item);
  }
  return isset($item['data']['durada_segs']) ? intval($item['data']['durada_segs']) : 0;
}

/**
 *  Implement hook_emvideo_PROVIDER_content_generate().
 */
function emvideo_tv3_content_generate() {
  return array(
    'http://www.tv3.cat/3alacarta/#/videos/1448530',
    'http://www.tv3.cat/3alacarta/#/videos/1448057',
    'http://www.tv3.cat/3alacarta/#/videos/1726952',
  );
}

/**
 * Implementation of hook_emfield_subtheme.
 */
function emvideo_tv3_emfield_subtheme() {
    return array(
        'emvideo_tv3_flash'  => array(
            'arguments' => array('embed' => NULL, 'width' => NULL, 'height' => NULL, 'autoplay' => NULL),
            'file' => 'providers/tv3.inc'
        ),
    );
}

/**
 * the embedded flash displaying the tv3 video
 */
function theme_emvideo_tv3_flash($embed, $width, $height, $autoplay, $options = array()) {
  static $counter;
  if ($embed) {
    $counter++;
    $autoplay = isset($options['autoplay']) ? $options['autoplay'] : $autoplay;
    $autoplay_value = $autoplay ? 'true' : 'false';

    $output .=
        "<object classid='clsid:d27cdb6e-ae6d-11cf-96b8-444553540000' " .
            "width='" . $width . "' " .
            "height='" . $height . "' " .
            "id='EVP" . $embed . "IE'>" .
            "<param name='movie' value='http://www.tv3.cat/ria/players/3ac/evp/Main.swf'></param>" .
            "<param name='scale' value='noscale'></param>" .
            "<param name='align' value='tl'></param>" .
            "<param name='swliveconnect' value='true'></param>" .
            "<param name='menu' value='true'></param>" .
            "<param name='allowFullScreen' value='true'></param>" .
            "<param name='allowScriptAccess' value='always'></param>" .
            "<param name='wmode' value='transparent'></param>" .
            "<param name='FlashVars' value='basepath=" .
                 "http://www.tv3.cat/ria/players/3ac/evp/&" .
                    "mesi=true&" .
                    "instancename=playerEVP_0&" .
                    "hassinopsi=true&" .
                    "autostart=" . $autoplay_value . "&" .
                    "comentaris=true&" .
                    "backgroundColor=000000&" .
                    "opcions=true&" .
                    "hasinsereix=true&" .
                    "refreshlock=true&" .
                    "minimal=false&" .
                    "subtitols=true&" .
                    "relacionats_canals=true&" .
                    "haspodcast=true&" .
                    "themepath=themes/evp_advanced.swf&" .
                    "hascomparteix=true&" .
                    "controlbar=true&" .
                    "hasenvia=true&" .
                    "hasrss=true&" .
                    "votacions=true&" .
                    "videoid=" . $embed . "&" .
                    "relacionats=true&" .
                    "basepath=http://www.tv3.cat/ria/players/3ac/evp/&" .
                    "xtm=true'></param>" .
            "<embed " .
                "width='" . $width . "' " .
                "height='" . $height . "' " .
                "type='application/x-shockwave-flash' " .
                "src='http://www.tv3.cat/ria/players/3ac/evp/Main.swf' " .
                "id='EVP" . $embed . "' " .
                "scale='noscale' " .
                "name='EVP" . $embed . "' " .
                "salign='tl' " .
                "swliveconnect='true' " .
                "menu='true' " .
                "allowfullscreen='true' " .
                "allowscriptaccess='always' " .
                "wmode='transparent' " .
                "FlashVars='" .
                    "basepath=http://www.tv3.cat/ria/players/3ac/evp/&" .
                    "mesi=true&" .
                    "instancename=playerEVP_0&" .
                    "hassinopsi=true&" .
                    "autostart=" . $autoplay_value . "&" .
                    "comentaris=true&" .
                    "backgroundColor=000000&" .
                    "opcions=true&" .
                    "hasinsereix=true&" .
                    "refreshlock=true&" .
                    "minimal=false&" .
                    "subtitols=true&" .
                    "relacionats_canals=true&" .
                    "haspodcast=true&" .
                    "themepath=themes/evp_advanced.swf&" .
                    "hascomparteix=true&" .
                    "controlbar=true&" .
                    "hasenvia=true&" .
                    "hasrss=true&" .
                    "votacions=true&" .
                    "videoid=" . $embed . "&" .
                    "relacionats=true&" .
                    "basepath=http://www.tv3.cat/ria/players/3ac/evp/&" .
                    "xtm=true'>" .
            "</embed>" .
        "</object>";
//    $output .= "<object type='application/x-shockwave-flash' height='$height' width='$width' data='http://www.tv3.cat/3alacarta/#/videos/$embed' id='tv3-video-$counter' ><param name='movie' value='http://www.tv3.cat/3alacarta/#/videos/$embed' /><param name='allowScriptAccess' value='always' /><param name='quality' value='best' /><param name='scale' value='noScale' /><param name='wmode' value='transparent' /><param name='FlashVars' value='autoplay=$autoplay_value' /><param name='allowfullscreen' value='true' /></object>";
  }
  return $output;
}
